<?php

require_once 'fonksiyon.php';
function tema_kategoriler() {
    $query = query("SELECT * FROM kategoriler ORDER BY kategori_adi ASC");

    while ($row = row($query)) {
        echo '<li><a href="' . URL . '/FONBARA/kategori/' . $row["kategori_link"] . '">' . ss($row["kategori_adi"]) . '</a></li>';
    }
}

function tema_icerik() {
    $do = g("do");

    switch ($do) {

        case "konu":
            if($link=  g("link")){
                echo $link;
            }else{
                go(URL."/FONBARA/");
            }
            break;
        case "kategori":
            if($link=  g("link")){
                echo $link;
            }else{
                go(URL."/FONBARA/");
            }
            break;
        case "uye":
            break;
        case "giris":
            
            if(session("login")){
                go(URL."/FONBARA");
            }else{
                require_once TEMA.'/giris.php';
            }
            break;
        case "cikis":
            break;
        default :
            require_once TEMA . '/default.php';
            break;
    }
}

function tema_anasayfa_konu() {
    $sayfa = g("s") ? g("s") : 1;
    $ksayisi = rows(query("SELECT * FROM konular WHERE konu_onay=1 && konu_anasayfa=1"));

    if (mysql_affected_rows()) {
        $limit = 3;
        $ssayisi = ceil($ksayisi / $limit);
        $baslangic = ($sayfa * $limit) - $limit;
        $query = query("SELECT * FROM konular INNER JOIN uyeler ON uyeler.uye_id = konular.konu_ekleyen INNER JOIN kategoriler ON kategoriler.kategori_id = konular.konu_kategori WHERE konu_onay=1 && konu_anasayfa=1 ORDER BY konu_id DESC LIMIT $baslangic,$limit");
        while ($row = row($query)) {
            $konu = explode("---", $row["konu_aciklama"]);
            $konu = $konu[0];
            $baslik = ss($row["konu_baslik"]);
            $link = URL . "/FONBARA/" . $row["konu_link"] . ".html";
            $katlink = URL . "/FONBARA/kategori/" . $row["kategori_link"];
            $kategori = ss($row["kategori_adi"]);
            $okunma = number_format($row["konu_hit"]);
            $tarihExplode = explode(" ", $row["konu_tarih"]);
            $tarih = $tarihExplode[0];
            $zaman = $tarihExplode[1];
            require TEMA . '/konu_anasayfa.php';
        }
    } else {
        $hata = "Buraya Henüz Hiç İçerik Eklenmemiş!";
        require_once TEMA . '/hata.php';
    }
}

function tema_anasayfa_sayfalama() {
    $sayfa = g("s") ? g("s") : 1;
    $ksayisi = rows(query("SELECT * FROM konular WHERE konu_onay=1 && konu_anasayfa=1"));
    $limit = 3;
    $ssayisi = ceil($ksayisi / $limit);
    echo "KAYIT SAYISI ".$ksayisi."</br>";
    echo "Sayfa SAYISI ".$ssayisi;
    if($ksayisi>$limit){
        ##ÖNCEKİ SAYFA
        $oncekiSayfa = $sayfa > 1 ? $sayfa - 1 : 1 ;
        $onceki = URL.'/FONBARA/index.php/sayfa/'.$oncekiSayfa;
        
        ##SONRAKİ SAYFA
        $sonrakiSayfa = $sayfa < $ssayisi ? $sayfa + 1 : $ssayisi ;
        $sonraki = URL.'/FONBARA/index.php/sayfa/'.$sonrakiSayfa;
        
        require_once TEMA.'/sayfala.php'; 
    }
    
}
?>