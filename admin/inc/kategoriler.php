<!--Güvenlik AMAÇLI ADMIN DEFINE TANIMLAMASI doğrudan link erişimini engeller-->
<?php echo !defined("ADMIN") ? die("HOP BİLADER HAYIRDIR...")  : null; ?>
   
<meta charset="utf-8">
<article  class="module width_3_quarter" style="width:95%;padding-bottom: 20px;">
		<header>
                    <div style="float:right;font-size:14px;font-weight: bold;padding:6px 10px; ">
                        <a href="<?php echo URL;?>/FONBARA/admin/index.php?do=kategori_ekle">Kategori Ekle</a>
                    </div>
                        <h3 class="tabs_involved">
                        KATEGORİ LİSTESİ
                    </h3></header>
		<div class="tab_container">
                    
                    <?php
                        $query = query("SELECT * FROM kategoriler ORDER BY kategori_id DESC");
                        if(mysql_affected_rows()){
                           ##kategoriler tablosu doluysa##
                    ?>
			<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   				<th width="20px"></th> 
    				<th>Kategori Adı</th> 
    				<th>Tarih</th> 
    				<th>İşlemler</th> 
				</tr> 
			</thead> 
			<tbody> 
                            <?php
                                while($row = row($query)){
                            ?>
				<tr> 
   				<td><input type="checkbox"></td> 
                                <td><?php echo ss($row["kategori_adi"]); ?></td>  
    				<td><?php echo $row["kategori_tarih"]; ?></td> 
    				<td>
                                    <a href="<?php echo URL ;?>/FONBARA/admin/index.php?do=kategori_duzenle&id=<?php echo $row["kategori_id"]; ?> " title="Düzenle"><img src="images/icn_edit.png" alt="Düzenle"/></a>
                                    <a onclick="return confirm('Kategoriyi Silmek İstediğinize Emin misiniz?')" href="<?php echo URL ;?>/FONBARA/admin/index.php?do=kategori_sil&id=<?php echo $row["kategori_id"]; ?> " title="Sil" style="margin-left: 10px"><img src="images/icn_trash.png" alt="Sil"/></a>
                                </td> 
				</tr> 
                            <?php } ?>
			</tbody> 
			</table>
			</div><!-- end of #tab1 --> 
                        <!-- eğer kategoriler tablosu boş ise-->
                        <?php }else{ ?> 
                        <h4 class="alert_warning">Siteye Henüz hiç kategori eklenmemiştir.</h4>
                        <?php } ?>
                        
		</div><!-- end of .tab_container -->
		
		</article><!-- end of content manager article -->
