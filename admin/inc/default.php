<meta charset="UTF-8">
<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
<?php echo !defined("ADMIN") ? die("HOP BİLADER Linkten gel...")  : null; ?>
        <header id="header">
		<hgroup>
                    <h1 class="site_title"><a href="<?php echo URL ;?>/FONBARA/admin">FONBARA </a></h1>
                        <h2 class="section_title">Admin Paneli</h2><div class="btn_view_site"><a href="<?php echo URL."/FONBARA/index.php";?>">Siteyi Göster</a></div>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
                    <p> <?php echo session("uye_kadi");?> (<a href="#">3 Mesaj Var</a>)</p>
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="index.php">Site Admin</a> <div class="breadcrumb_divider"></div> <a class="current">Ana Panel</a></article>
		</div>
	</section><!-- end of secondary bar -->
	
	<aside id="sidebar" class="column">
		<form class="quick_search">
			<input type="text" value="Hızlı Arama" onfocus="if(!this._haschanged){this.value=''};this._haschanged=true;">
		</form>
		<hr/>
		<h3>İÇERİK</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="?do=icerik_ekle">İçerik Ekle</a></li>
			<li class="icn_edit_article"><a href="?do=icerikler">İçeriği Düzenle</a></li>
			<li class="icn_edit_article"><a href="?do=onay_bekleyen_icerikler">Onay Bekleyen İçerikler</a></li>
			 
		</ul>
		<h3>KULLANICILAR</h3>
		<ul class="toggle">
			<li class="icn_add_user"><a href="?do=uye_ekle">Yeni Kullanıcı Ekle</a></li>
			<li class="icn_view_users"><a href="?do=uyeler">Kullanıcıları Düzenle</a></li>
			<li class="icn_view_users"><a href="?do=onay_bekleyen_uyeler">Onay Bekleyen Kullanıcılar</a></li>
		</ul>
		<h3>KATEGORİLER</h3>
		<ul class="toggle">
			<li class="icn_folder"><a href="?do=kategori_ekle">Yeni Kategori Ekle</a></li>
			<li class="icn_photo"><a href="?do=kategoriler">Kategorileri Düzenle</a></li>
		</ul>
		<h3>Admin</h3>
		<ul class="toggle">
			<li class="icn_settings"><a href="?do=ayarlar">Genel Ayarlar</a></li> 
			<li class="icn_jump_back"><a href="?do=cikis_yap">Çıkış Yap</a></li>
		</ul>
		
		<footer>
			<hr />
			<p><strong>Copyright &copy; 2014 BLOG UYGULAMASI </strong></p>
			<p>Theme by <a href="http://www.medialoot.com">MediaLoot</a></p>
		</footer>
	</aside><!-- end of sidebar -->
	
	<section id="main" class="column">
            <!-- SAYFALAR ARASI LİNKLEME SİSTEMİ  -->
            <?php
                    $do = g("do");
                    if (file_exists("inc/{$do}.php")){
                        require("inc/{$do}.php");
                    }else{
                        require("inc/anasayfa.php");
                    }
            ?>
		
	</section>




 

