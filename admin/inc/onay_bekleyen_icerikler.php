<!--Güvenlik AMAÇLI ADMIN DEFINE TANIMLAMASI doğrudan link erişimini engeller-->
<?php echo !defined("ADMIN") ? die("HOP BİLADER HAYIRDIR...")  : null; ?>
   
<meta charset="utf-8">
<article  class="module width_3_quarter" style="width:95%;padding-bottom: 20px;">
		<header>
                    <div style="float:right;font-size:14px;font-weight: bold;padding:6px 10px; ">
                        <a href="<?php echo URL;?>/FONBARA/admin/index.php?do=icerik_ekle">İçerik Ekle</a>
                    </div>
                        <h3 class="tabs_involved">
                        ONAY BEKLEYEN İÇERİKLER LİSTESİ
                    </h3></header>
		<div class="tab_container">
                    
                    <?php
                        $sayfa = g("s") ? g("s") : 1;
                        $ksayisi = rows(query("SELECT konu_id FROM konular WHERE konu_onay=0 ")); 
                        
                        $limit = 10; //her sayfa kaçar adetse limit odur  //
                        $ssayisi = ceil($ksayisi / $limit );
                        $baslangic =($sayfa * $limit) - $limit; 
                        $query = query("SELECT * FROM konular INNER JOIN uyeler ON uyeler.uye_id = konular.konu_ekleyen INNER JOIN kategoriler ON kategoriler.kategori_id = konular.konu_kategori WHERE konu_onay=0 ORDER BY konu_id DESC LIMIT $baslangic, $limit");
                        if(mysql_affected_rows()){
                           ##uyeler tablosu doluysa##
                    ?>
			<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   				<th width="5%"></th> 
    				<th width="40%">Başlık</th>  
    				<th width="10%">Ekleyen</th> 
    				<th width="10%">Kategori</th> 
    				<th>Tarih</th> 
    				<th width="10%">İşlemler</th> 
				</tr> 
			</thead> 
			<tbody> 
                            <?php
                                while($row = row($query)){
                            ?>
				<tr> 
   				<td><input type="checkbox"></td> 
                                <td><?php echo ss($row["konu_baslik"]); ?></td>    
                                <td><a href="<?php echo URL; ?>/FONBARA/admin/index.php?do=uye_duzenle&id=<?php echo $row["uye_id"]; ?>"><?php echo ss($row["uye_kadi"]); ?></a></td>    
                                <td><a href="<?php echo URL; ?>/FONBARA/admin/index.php?do=kategori_duzenle&id=<?php echo $row["kategori_id"]; ?>"><?php echo $row["kategori_adi"]; ?></a></td> 
    				<td><?php echo $row["konu_tarih"]; ?></td> 
    				<td>
                                    <a href="<?php echo URL ;?>/FONBARA/admin/index.php?do=icerik_duzenle&id=<?php echo $row["konu_id"]; ?> " title="Düzenle"><img src="images/icn_edit.png" alt="Düzenle"/></a>
                                    <a onclick="return confirm('Konuyu Silmek İstediğinize Emin misiniz?')" href="<?php echo URL ;?>/FONBARA/admin/index.php?do=icerik_sil&id=<?php echo $row["konu_id"]; ?> " title="Sil" style="margin-left: 10px"><img src="images/icn_trash.png" alt="Sil"/></a>
                                </td> 
				</tr> 
                            <?php } ?>
			</tbody> 
			</table>
                            <?php if ($ksayisi>$limit){ ?>
                            <form action="" method="get">
                                <input type="hidden" value="<?php echo g("do"); ?>" name="do" />
                                <ul class="sayfala">
                                <li><select name="s">
                                    <?php
                                        for($i=1;$i<=$ssayisi;$i++){
                                            echo '<option  ';
                                            
                                            echo $i == $sayfa ? 'selected' : null;
                                            
                                            echo ' value="'.$i.'">'.$i.'. Sayfa </option>' ;
                                        }
                                    ?>                                
                                    </select></li>
                                    <li><button type="submit">GÖSTER</button></li>
                                </ul>
                            </form>
                            <?php } ?>
                            
			</div><!-- end of #tab1 --> 
                        <!-- eğer kategoriler tablosu boş ise-->
                        <?php }else{ ?> 
                        <h4 class="alert_warning">Henüz onaylanmamış bir konu eklenmemiştir.</h4>
                        <?php } ?>
                        
		</div><!-- end of .tab_container -->
		
		</article><!-- end of content manager article -->
