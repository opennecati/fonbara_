-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 12 Ağu 2014, 16:31:46
-- Sunucu sürümü: 5.6.16
-- PHP Sürümü: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Veritabanı: `blog`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `ayarlar`
--

CREATE TABLE IF NOT EXISTS `ayarlar` (
  `site_url` varchar(250) CHARACTER SET utf8 NOT NULL,
  `site_baslik` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `site_desc` varchar(300) COLLATE utf8_turkish_ci NOT NULL,
  `site_keyw` varchar(300) CHARACTER SET utf8 NOT NULL,
  `site_tema` varchar(150) CHARACTER SET utf8 NOT NULL,
  `site_durum` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `ayarlar`
--

INSERT INTO `ayarlar` (`site_url`, `site_baslik`, `site_desc`, `site_keyw`, `site_tema`, `site_durum`) VALUES
('http://localhost', 'kodlamasi.tk', 'YAZULUM MAZULUM', 'keyword (anahtar kelimeler)', 'default', 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `kategoriler`
--

CREATE TABLE IF NOT EXISTS `kategoriler` (
  `kategori_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_adi` varchar(200) COLLATE utf8_turkish_ci NOT NULL,
  `kategori_tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `kategori_link` varchar(200) COLLATE utf8_turkish_ci NOT NULL,
  `kategori_desc` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `kategori_keyw` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `kategori_anasayfa_konu` varchar(100) COLLATE utf8_turkish_ci NOT NULL,
  `kategori_full_konu` varchar(100) COLLATE utf8_turkish_ci NOT NULL,
  PRIMARY KEY (`kategori_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=2 ;

--
-- Tablo döküm verisi `kategoriler`
--

INSERT INTO `kategoriler` (`kategori_id`, `kategori_adi`, `kategori_tarih`, `kategori_link`, `kategori_desc`, `kategori_keyw`, `kategori_anasayfa_konu`, `kategori_full_konu`) VALUES
(1, 'Şeker Şey', '2014-08-12 13:55:02', 'seker-sey', '', '', '', '');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `uyeler`
--

CREATE TABLE IF NOT EXISTS `uyeler` (
  `uye_id` int(11) NOT NULL AUTO_INCREMENT,
  `uye_kadi` varchar(200) COLLATE utf8_turkish_ci NOT NULL,
  `uye_sifre` varchar(200) COLLATE utf8_turkish_ci NOT NULL,
  `uye_eposta` varchar(200) COLLATE utf8_turkish_ci NOT NULL,
  `uye_tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `uye_rutbe` int(11) NOT NULL,
  `uye_onay` int(11) NOT NULL,
  PRIMARY KEY (`uye_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=3 ;

--
-- Tablo döküm verisi `uyeler`
--

INSERT INTO `uyeler` (`uye_id`, `uye_kadi`, `uye_sifre`, `uye_eposta`, `uye_tarih`, `uye_rutbe`, `uye_onay`) VALUES
(1, 'omuralcin', 'e10adc3949ba59abbe56e057f20f883e ', 'omuralcin@gmail.com', '2014-08-07 14:29:40', 1, 1),
(2, 'moderator', '12345678', 'moderator@blog.com', '2014-08-11 08:19:17', 1, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
