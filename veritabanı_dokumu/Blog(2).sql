-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 11, 2014 at 02:11 AM
-- Server version: 5.1.73
-- PHP Version: 5.3.2-1ubuntu4.26

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `Blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `ayarlar`
--

CREATE TABLE IF NOT EXISTS `ayarlar` (
  `site_url` varchar(250) CHARACTER SET utf8 NOT NULL,
  `site_baslik` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `site_desc` varchar(300) COLLATE utf8_turkish_ci NOT NULL,
  `site_keyw` varchar(300) CHARACTER SET utf8 NOT NULL,
  `site_tema` varchar(150) CHARACTER SET utf8 NOT NULL,
  `site_durum` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Dumping data for table `ayarlar`
--

INSERT INTO `ayarlar` (`site_url`, `site_baslik`, `site_desc`, `site_keyw`, `site_tema`, `site_durum`) VALUES
('http://localhost', 'kodlamasi.tk', 'YAZULUM ve MAZULUM', 'Linux , OpenSource , j2ee , Java', 'default', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kategoriler`
--

CREATE TABLE IF NOT EXISTS `kategoriler` (
  `kategori_id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_adi` varchar(200) COLLATE utf8_turkish_ci NOT NULL,
  `kategori_tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `kategori_link` varchar(200) COLLATE utf8_turkish_ci NOT NULL,
  `kategori_desc` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `kategori_keyw` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `kategori_anasayfa_konu` varchar(100) COLLATE utf8_turkish_ci NOT NULL,
  `kategori_full_konu` varchar(100) COLLATE utf8_turkish_ci NOT NULL,
  PRIMARY KEY (`kategori_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `kategoriler`
--

INSERT INTO `kategoriler` (`kategori_id`, `kategori_adi`, `kategori_tarih`, `kategori_link`, `kategori_desc`, `kategori_keyw`, `kategori_anasayfa_konu`, `kategori_full_konu`) VALUES
(1, 'Åžeker ÅŸey', '2014-08-12 16:55:02', 'seker-sey', 'deneme', 'deneme', 'deneme', 'deneme'),
(3, 'SabÄ±r', '2014-08-23 22:30:42', 'sabir', 'SabÄ±r Ã¶rnekleri.', 'sabÄ±r,umut,cesaret', 'SabÄ±rlÄ± Olmak', 'SabÄ±rlÄ± Olmak');

-- --------------------------------------------------------

--
-- Table structure for table `konular`
--

CREATE TABLE IF NOT EXISTS `konular` (
  `konu_id` int(11) NOT NULL AUTO_INCREMENT,
  `konu_baslik` varchar(300) COLLATE utf8_turkish_ci NOT NULL,
  `konu_link` varchar(300) COLLATE utf8_turkish_ci NOT NULL,
  `konu_aciklama` text COLLATE utf8_turkish_ci NOT NULL,
  `konu_etiket` varchar(500) COLLATE utf8_turkish_ci NOT NULL,
  `konu_ekleyen` int(11) NOT NULL,
  `konu_tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `konu_kategori` int(11) NOT NULL,
  `konu_onay` int(11) NOT NULL,
  `konu_anasayfa` int(11) NOT NULL,
  `konu_hit` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`konu_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `konular`
--

INSERT INTO `konular` (`konu_id`, `konu_baslik`, `konu_link`, `konu_aciklama`, `konu_etiket`, `konu_ekleyen`, `konu_tarih`, `konu_kategori`, `konu_onay`, `konu_anasayfa`, `konu_hit`) VALUES
(1, 'AÅŸk', 'ask', 'AÅŸk nedir?AÅŸk hakkÄ±nda...---AÅŸkÄ±n gÃ¶zÃ¼ kÃ¶rdÃ¼r falan derler ozaman neden aÅŸÄ±k olur insan.Ä°nsan kÃ¶r mÃ¼dÃ¼r?', 'aÅŸk,meÅŸk,falan', 1, '2014-08-24 02:26:53', 1, 1, 1, 0),
(2, 'MeÅŸk', 'mesk', 'meÅŸk hakkÄ±nda---MeÅŸk nedir', 'aÅŸkimeÅŸk,falan', 1, '2014-08-24 02:27:57', 1, 1, 1, 0),
(3, 'TevekkÃ¼l', 'tevekkul', 'TevekkÃ¼l nedir?TevekkÃ¼l ne zaman yapÄ±lÄ±r?---TevekkÃ¼l allaha tam bir teslimiyet ile gÃ¼venmektir.Bir nevi iÅŸini saÄŸlamlaÅŸtÄ±rmaktÄ±r da diyebiliriz.', 'tevekkÃ¼l,iman,teslimiyet', 1, '2014-08-24 04:12:07', 3, 1, 1, 0),
(6, 'RÄ±za Kazanmak', 'riza-kazanmak', 'AllahÄ±n rÄ±zasÄ±nÄ± kazanmanÄ±n yollarÄ±.---AllahÄ±n rÄ±zasÄ±nÄ±n olduÄŸu iÅŸlere gÃ¶nÃ¼lden meyil etmek allahÄ±n da isteÄŸi ile rÄ±zasÄ±nÄ± kazanmaya vesile olurlar.', 'rÄ±za,sabÄ±r,iman', 1, '2014-08-24 17:07:11', 3, 1, 1, 0),
(7, 'SELAMLAÅžMAK', 'selamlasmak', 'SelamÄ± aranÄ±zda yayÄ±nÄ±z.Selam sÃ¼nnettir.SÃ¼nnetler Allah''a yakÄ±nlaÅŸtÄ±rÄ±r.---SelamÄ± aranÄ±zda yayÄ±nÄ±z.Selam sÃ¼nnettir.SÃ¼nnetler Allah''a yakÄ±nlaÅŸtÄ±rÄ±r.Farzlarda olduÄŸu kadar sÃ¼nnetlerde de dikkatli olmak gerektir ki bÃ¶ylece Allah-Ã¼ Teala''nÄ±n RÄ±zasÄ± kazanÄ±lmasÄ± umulur.', 'selamlasmak,sunnetler,allah rÄ±zasÄ±', 1, '2014-08-30 04:53:06', 3, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `uyeler`
--

CREATE TABLE IF NOT EXISTS `uyeler` (
  `uye_id` int(11) NOT NULL AUTO_INCREMENT,
  `uye_kadi` varchar(200) COLLATE utf8_turkish_ci NOT NULL,
  `uye_link` varchar(200) COLLATE utf8_turkish_ci NOT NULL,
  `uye_sifre` varchar(200) COLLATE utf8_turkish_ci NOT NULL,
  `uye_eposta` varchar(200) COLLATE utf8_turkish_ci NOT NULL,
  `uye_sehir` varchar(200) COLLATE utf8_turkish_ci NOT NULL,
  `uye_ulke` varchar(200) COLLATE utf8_turkish_ci NOT NULL,
  `uye_tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `uye_rutbe` int(11) NOT NULL,
  `uye_onay` int(11) NOT NULL,
  PRIMARY KEY (`uye_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `uyeler`
--

INSERT INTO `uyeler` (`uye_id`, `uye_kadi`, `uye_link`, `uye_sifre`, `uye_eposta`, `uye_sehir`, `uye_ulke`, `uye_tarih`, `uye_rutbe`, `uye_onay`) VALUES
(1, 'omuralcin', 'omuralcin', 'e10adc3949ba59abbe56e057f20f883e ', 'omuralcin@gmail.com', '', '', '2014-08-07 17:29:40', 1, 1),
(2, 'moderator', 'moderator', 'e10adc3949ba59abbe56e057f20f883e ', 'moderator@blog.com', '', '', '2014-08-11 11:19:17', 1, 1),
(3, 'sami1', 'sami1', 'e10adc3949ba59abbe56e057f20f883e', 'sami1@gmail.com', '', '', '2014-08-23 16:36:21', 1, 1),
(6, 'hamdi', 'hamdi', 'e10adc3949ba59abbe56e057f20f883e', 'hamdi@gmail.com', '', '', '2014-08-23 20:27:04', 2, 1),
(7, 'keriman', 'keriman', 'e10adc3949ba59abbe56e057f20f883e', 'keriman@gmail.com', '', '', '2014-08-23 20:27:52', 2, 1),
(8, 'ahmet', 'ahmet', 'e10adc3949ba59abbe56e057f20f883e', 'ahmet@gmail.com', 'İstanbul', 'Türkiye', '2014-08-23 22:54:30', 0, 0),
(9, 'kemal', 'kemal', 'e10adc3949ba59abbe56e057f20f883e', 'kemal@gmail.com', 'İstanbul', 'Türkiye', '2014-08-23 23:01:44', 1, 0);
